package com.atlassian.hipchat.test.api;

import org.simpleframework.http.Response;
import org.simpleframework.http.Status;

public final class HipChatResponse
{
    private final Status status;
    private final String content;

    private HipChatResponse(Status status, String content)
    {
        this.status = status;
        this.content = content;
    }

    public static HipChatResponse ok(String content)
    {
        return status(Status.OK, content);
    }

    public static HipChatResponse unauthorized(String content)
    {
        return status(Status.UNAUTHORIZED, content);
    }

    public static HipChatResponse serverError(String content)
    {
        return status(Status.INTERNAL_SERVER_ERROR, content);
    }

    public static HipChatResponse badRequest(String content)
    {
        return status(Status.BAD_REQUEST, content);
    }

    private static HipChatResponse status(Status status, String content)
    {
        return new HipChatResponse(status, content);
    }

    void respond(Response resp)
    {
        HipChatServer.respond(resp, status, content);
    }
}
