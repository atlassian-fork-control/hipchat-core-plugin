package com.atlassian.hipchat.test.api;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.hipchat.plugins.api.client.ClientError;
import com.atlassian.hipchat.plugins.api.client.HipChatClient;
import com.atlassian.hipchat.plugins.api.client.Room;
import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import com.atlassian.hipchat.plugins.core.client.HipChatClientImpl;
import com.atlassian.sal.api.ApplicationProperties;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.hipchat.plugins.core.client.ClientUtils.urlEncode;
import static com.atlassian.hipchat.test.api.HipChatRequest.get;
import static com.atlassian.hipchat.test.api.HipChatRequest.post;
import static com.atlassian.hipchat.test.api.HipChatResponse.badRequest;
import static com.atlassian.hipchat.test.api.HipChatResponse.ok;
import static com.atlassian.hipchat.test.api.HipChatResponse.serverError;
import static com.atlassian.hipchat.test.api.HipChatResponse.unauthorized;
import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestHipChatClient
{
    private HipChatClient hipChatClient;
    private HipChatServer hipChatServer;

    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private HipChatConfigurationManager hipChatConfigurationManager;

    @Before
    public void setUp()
    {
        hipChatServer = HipChatServer.start(Option.<Integer>none());

        when(hipChatConfigurationManager.getApiBaseUrl()).thenReturn(format("http://localhost:%s", hipChatServer.getPort()));
        when(hipChatConfigurationManager.getApiToken()).thenReturn(some("token"));
        hipChatClient = new HipChatClientImpl(eventPublisher, applicationProperties, hipChatConfigurationManager);
    }

    @After
    public void tearDown()
    {
        hipChatServer.stop();
    }

    @Test
    public void canAuth()
    {
        hipChatServer.handle(get("/v1/"), ok(""));

        final Either<ClientError, Boolean> canAuth = hipChatClient.canAuthenticate(Option.<String>none()).claim();
        assertTrue(canAuth.isRight());
        assertTrue(canAuth.right().get());
    }

    @Test
    public void cannotAuth()
    {
        hipChatServer.handle(get("/v1/"), unauthorized(""));

        final Either<ClientError, Boolean> canAuth = hipChatClient.canAuthenticate(Option.<String>none()).claim();
        assertTrue(canAuth.isRight());
        assertFalse(canAuth.right().get());
    }

    @Test
    public void canAuthServerError()
    {
        final String code = "500";
        final String type = "Internal Server Error";
        final String message = "There was a problem on the server.";

        hipChatServer.handle(get("/v1/"), serverError(error(code, type, message)));

        final Either<ClientError, Boolean> canAuth = hipChatClient.canAuthenticate(Option.<String>none()).claim();

        assertError(code, type, message, canAuth);
    }

    @Test
    public void listWithNoRooms()
    {
        hipChatServer.handle(get("/v1/rooms/list"), ok("{\"rooms\":[]}"));

        final Either<ClientError, List<Room>> rooms = hipChatClient.rooms().list().claim();
        assertTrue(rooms.isRight());
        assertTrue(rooms.right().get().isEmpty());
    }

    @Test
    public void listWithRooms()
    {
        final Map<String, Object> room1 = ImmutableMap.<String, Object>builder()
                .put("room_id", 7l)
                .put("name", "Development")
                .put("topic", "Make sure to document your API functions well!")
                .put("last_active", 1269020400l)
                .put("created", 1269010311l)
                .put("owner_user_id", 1l)
                .put("is_archived", false)
                .put("is_private", false)
                .put("xmpp_jid", "7_development@conf.hipchat.com")
                .build();

        final Map<String, Object> room2 = ImmutableMap.<String, Object>builder()
                .put("room_id", 10l)
                .put("name", "Ops")
                .put("topic", "Chef is so awesome.")
                .put("last_active", 1269010500l)
                .put("created", 1269010211l)
                .put("owner_user_id", 5l)
                .put("is_archived", false)
                .put("is_private", true)
                .put("xmpp_jid", "10_ops@conf.hipchat.com")
                .build();

        hipChatServer.handle(get("/v1/rooms/list"), ok("{\"rooms\": [" + room(room1) + "," + room(room2) + "]}"));

        final Either<ClientError, List<Room>> roomsEither = hipChatClient.rooms().list().claim();
        assertTrue(roomsEither.isRight());
        final List<Room> rooms = roomsEither.right().get();
        assertEquals(2, rooms.size());

        assertRoom(rooms.get(0), room1);
        assertRoom(rooms.get(1), room2);
    }

    @Test
    public void createRoom()
    {
        final String name = "Test Room";
        final long ownerId = 1l;

        final Map<String, Object> room = ImmutableMap.<String, Object>builder()
                .put("room_id", 19l)
                .put("name", name)
                .put("topic", "")
                .put("last_active", 0l)
                .put("created", 1269010311l)
                .put("owner_user_id", ownerId)
                .put("is_archived", false)
                .put("is_private", false)
                .put("xmpp_jid", "19_blah@conf.hipchat.com")
                .put("participants", ImmutableList.of())
                .put("guest_access_url", "null")
                .build();

        hipChatServer.handle(post("/v1/rooms/create", urlEncode(ImmutableMap.of("name", name, "owner_user_id", String.valueOf(ownerId)))), ok("{\"room\": " + room(room) + "}"));

        final Either<ClientError, Room> roomEither = hipChatClient.rooms().create(name, ownerId, Option.<Boolean>none(), Option.<String>none(), Option.<Boolean>none()).claim();

        assertTrue(roomEither.isRight());
        final Room r = roomEither.right().get();
        assertEquals(name, r.getName());
        assertEquals(ownerId, r.getOwnerId());
    }

    @Test
    public void createRoomBadRequest()
    {
        final String code = "400";
        final String type = "Bad Request";
        final String message = "Missing required parameter: ...";

        final String name = "Test Room";
        final long ownerId = 1l;

        hipChatServer.handle(post("/v1/rooms/create", urlEncode(ImmutableMap.of("name", name, "owner_user_id", String.valueOf(ownerId)))), badRequest(error(code, type, message)));

        final Either<ClientError, Room> room = hipChatClient.rooms().create(name, ownerId, Option.<Boolean>none(), Option.<String>none(), Option.<Boolean>none()).claim();
        assertError(code, type, message, room);
    }

    private static void assertError(String code, String type, String message, Either<ClientError, ?> either)
    {
        assertTrue(either.isLeft());
        final ClientError error = either.left().get();
        assertEquals(code, error.getCode().get());
        assertEquals(type, error.getType().get());
        assertEquals(message, error.getMessage());
    }


    private static void assertRoom(Room room, Map<String, Object> r)
    {
        assertEquals(r.get("room_id"), room.getId());
        assertEquals(r.get("name"), room.getName());
        assertEquals(r.get("topic"), room.getTopic());
        assertEquals(r.get("last_active"), room.getLastActive().getTime() / 1000);
        assertEquals(r.get("created"), room.getCreated().getTime() / 1000);
        assertEquals(r.get("owner_user_id"), room.getOwnerId());
        assertEquals(r.get("is_archived"), room.isArchived());
        assertEquals(r.get("is_private"), room.isPrivate());
        assertEquals(r.get("xmpp_jid"), room.getJid());
    }

    private static String room(Map<String, Object> r)
    {
        return "{\"room_id\": " + r.get("room_id") + "," +
                "\"name\": \"" + r.get("name") + "\"," +
                "\"topic\": \"" + r.get("topic") + "\"," +
                "\"last_active\": " + r.get("last_active") + "," +
                "\"created\": " + r.get("created") + "," +
                "\"owner_user_id\": " + r.get("owner_user_id") + "," +
                "\"is_archived\": " + r.get("is_archived") + "," +
                "\"is_private\": " + r.get("is_private") + "," +
                "\"participants\": " + participants((List<Map<String, Object>>) r.get("participants")) + "," +
                "\"guest_url_access\": " + r.get("guest_url_access") + "," +
                "\"xmpp_jid\": \"" + r.get("xmpp_jid") + "\"}";
    }

    private static String participants(List<Map<String, Object>> participants)
    {
        String json = "[";
        if (participants != null)
        {
            for (Map<String, Object> participant : participants)
            {
                json += participant(participant) + ", ";
            }
            if (!participants.isEmpty())
            {
                json = json.substring(0, json.length() - 2);
            }
        }
        return json + "]";
    }

    private static String participant(Map<String, Object> participant)
    {
        return "";
    }

    private static String error(String code, String type, String message)
    {
        return "{\"error\":{\"code\":" + code + ",\"type\":\"" + type + "\",\"message\":\"" + message + "\"}}";
    }
}
