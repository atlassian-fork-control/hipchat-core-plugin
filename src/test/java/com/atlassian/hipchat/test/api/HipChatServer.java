package com.atlassian.hipchat.test.api;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Pair;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.Status;
import org.simpleframework.http.core.Container;
import org.simpleframework.http.core.ContainerServer;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.fugue.Pair.pair;
import static com.google.common.base.Suppliers.ofInstance;

public final class HipChatServer
{
    private static final Logger logger = LoggerFactory.getLogger(HipChatServer.class);

    public static final int DEFAULT_PORT = 4000;

    private final HipChatContainer container;
    private final Connection connection;
    private final int port;

    private HipChatServer(HipChatContainer container, Connection connection, int port)
    {
        this.port = port;
        this.container = Preconditions.checkNotNull(container);
        this.connection = Preconditions.checkNotNull(connection);
    }

    public int getPort()
    {
        return port;
    }

    public static HipChatServer start(Option<Integer> port)
    {
        try
        {
            HipChatContainer container = new HipChatContainer();
            Connection connection = new SocketConnection(new ContainerServer(container));
            final int freePort = getFreePort(port);
            connection.connect(new InetSocketAddress(freePort));

            logger.info("HipChat 'server' started on port '{}'", freePort);
            return new HipChatServer(container, connection, freePort);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    private static int getFreePort(Option<Integer> port)
    {
        return isFreePort(port.getOrElse(DEFAULT_PORT))
                .orElse(new Supplier<Option<Integer>>()
                {
                    @Override
                    public Option<Integer> get()
                    {
                        return isFreePort(0);
                    }
                })
                .getOrError(ofInstance("Could not find free port"));
    }

    private static Option<Integer> isFreePort(int port)
    {
        ServerSocket socket = null;
        try
        {
            socket = new ServerSocket(port);
            return some(socket.getLocalPort());
        }
        catch (IOException e)
        {
            return none();
        }
        finally
        {
            closeQuietly(socket);
        }
    }

    private static void closeQuietly(ServerSocket socket)
    {
        try
        {
            if (socket != null)
            {
                socket.close();
            }
        }
        catch (IOException e)
        {
            logger.error("IOException should not have been thrown.", e);
        }
    }

    public void stop()
    {
        try
        {
            connection.close();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void handle(HipChatRequest request, HipChatResponse response)
    {
        // add in reverse order so that new entries get precedence over old ones.
        container.handles.add(0, pair(request, response));
    }

    public void clear()
    {
        container.handles.clear();
    }

    private static final class HipChatContainer implements Container
    {
        private final List<Pair<HipChatRequest, HipChatResponse>> handles = new CopyOnWriteArrayList<Pair<HipChatRequest, HipChatResponse>>();

        @Override
        public void handle(Request req, Response resp)
        {
            for (Pair<HipChatRequest, HipChatResponse> handle : handles)
            {
                if (handle.left().matches(req))
                {
                    handle.right().respond(resp);
                }
            }

            respond(resp, Status.NOT_FOUND, "c");
        }
    }

    static void respond(Response resp, Status status, String content)
    {
        resp.setStatus(status);
        try
        {
            final PrintStream printStream = resp.getPrintStream();
            printStream.print(content);
            printStream.flush();
            printStream.close();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
