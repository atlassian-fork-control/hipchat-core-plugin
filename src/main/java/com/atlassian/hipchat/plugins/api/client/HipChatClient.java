package com.atlassian.hipchat.plugins.api.client;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.util.concurrent.Promise;

public interface HipChatClient
{
    Promise<Either<ClientError, Boolean>> canAuthenticate(Option<String> token);

    HipChatRoomsClient rooms();

    HipChatUsersClient users();
}
