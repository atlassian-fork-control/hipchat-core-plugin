package com.atlassian.hipchat.plugins.api.config;

import com.atlassian.fugue.Option;

public interface HipChatConfigurationManager
{
    String getApiBaseUrl();

    void setApiBaseUrl(String baseUrl);

    Option<String> getApiToken();

    void setApiToken(String token);

    void set(String key, String value);

    Option<String> get(String key);
}
