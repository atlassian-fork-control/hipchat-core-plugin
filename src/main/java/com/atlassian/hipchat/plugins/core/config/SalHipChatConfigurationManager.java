package com.atlassian.hipchat.plugins.core.config;

import com.atlassian.fugue.Option;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import static com.atlassian.fugue.Option.option;
import static com.google.common.base.Preconditions.checkNotNull;

public final class SalHipChatConfigurationManager extends AbstractHipChatConfiguration
{
    private static final String PLUGIN_STORAGE_KEY = "com.atlassian.labs.hipchat";

    private final PluginSettingsFactory pluginSettingsFactory;

    public SalHipChatConfigurationManager(PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginSettingsFactory = checkNotNull(pluginSettingsFactory);
    }

    @Override
    protected void internalSet(String key, String value)
    {
        getSettings().put(key, value);
    }

    @Override
    protected Option<String> internalGet(String key)
    {
        return option((String) getSettings().get(key));
    }

    private PluginSettings getSettings()
    {
        return pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
    }
}
