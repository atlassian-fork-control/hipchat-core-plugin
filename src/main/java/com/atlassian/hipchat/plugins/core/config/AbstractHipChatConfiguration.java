package com.atlassian.hipchat.plugins.core.config;

import com.atlassian.fugue.Option;
import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import com.google.common.base.Function;
import com.google.common.base.Supplier;

import static com.atlassian.fugue.Option.option;
import static com.google.common.base.Preconditions.checkState;
import static java.lang.String.format;

abstract class AbstractHipChatConfiguration implements HipChatConfigurationManager
{
    private static final String API_BASE_URI = "https://api.hipchat.com";

    private static final String HIPCHAT_NODE_ENV_NAME = "hipchat_node";
    private static final String HIPCHAT_DEV_BASE_URL_FORMAT = "https://%s.hipchat.com/";

    private static final String HIPCHAT_AUTH_TOKEN_KEY = "hipchat-auth-token";
    private static final String HIPCHAT_BASE_URL_KEY = "hipchat-base-url";

    @Override
    public final String getApiBaseUrl()
    {
        return getConfiguredBaseUrl()
                .orElse(new Supplier<Option<String>>()
                {
                    @Override
                    public Option<String> get()
                    {
                        return getHipChatDevBaseUrl();
                    }
                })
                .getOrElse(API_BASE_URI).trim();
    }

    private Option<String> getConfiguredBaseUrl()
    {
        return internalGet(HIPCHAT_BASE_URL_KEY);
    }

    private Option<String> getHipChatDevBaseUrl()
    {
        if (isDevMode())
        {
            return getHipChatNodeName().map(new Function<String, String>()
            {
                @Override
                public String apply(String node)
                {
                    return format(HIPCHAT_DEV_BASE_URL_FORMAT, node);
                }
            });
        }
        else
        {
            return Option.none();
        }
    }

    private Option<String> getHipChatNodeName()
    {
        return option(System.getenv().get(HIPCHAT_NODE_ENV_NAME));
    }

    /**
     * Tells whether we're in dev mode. Products with specific flags can override this method.
     */
    protected boolean isDevMode()
    {
        return Boolean.getBoolean("atlassian.dev.mode");
    }

    @Override
    public final void setApiBaseUrl(String baseUrl)
    {
        internalSet(HIPCHAT_BASE_URL_KEY, baseUrl);
    }

    @Override
    public final Option<String> getApiToken()
    {
        return internalGet(HIPCHAT_AUTH_TOKEN_KEY);
    }

    @Override
    public final void setApiToken(String token)
    {
        internalSet(HIPCHAT_AUTH_TOKEN_KEY, token);
    }

    @Override
    public final void set(String key, String value)
    {
        checkState(!HIPCHAT_BASE_URL_KEY.equals(key));
        checkState(!HIPCHAT_AUTH_TOKEN_KEY.equals(key));
        internalSet(key, value);
    }

    @Override
    public final Option<String> get(String key)
    {
        checkState(!HIPCHAT_BASE_URL_KEY.equals(key));
        checkState(!HIPCHAT_AUTH_TOKEN_KEY.equals(key));
        return internalGet(key);
    }

    protected abstract void internalSet(String key, String value);

    protected abstract Option<String> internalGet(String key);
}
