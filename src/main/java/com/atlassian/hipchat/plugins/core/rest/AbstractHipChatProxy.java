package com.atlassian.hipchat.plugins.core.rest;

import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import java.net.URI;
import java.util.List;
import java.util.Map;

import com.google.common.base.Predicate;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.toArray;

public abstract class AbstractHipChatProxy
{
    private static final String AUTH_TOKEN_QUERY_PARAM = "auth_token";

    private static final Logger logger = LoggerFactory.getLogger(AbstractHipChatProxy.class);

    private final HipChatConfigurationManager configurationManager;
    private final RequestFactory<Request<?, com.atlassian.sal.api.net.Response>> requestFactory;
    private final String version;

    protected AbstractHipChatProxy(HipChatConfigurationManager configurationManager,
                                   RequestFactory<Request<?, com.atlassian.sal.api.net.Response>> requestFactory,
                                   String version)
    {
        this.configurationManager = checkNotNull(configurationManager);
        this.requestFactory = checkNotNull(requestFactory);
        this.version = checkNotNull(version);
    }

    @GET
    public final Response get(@Context UriInfo uriInfo, @Context HttpHeaders headers, @PathParam("path") String path)
    {
        final URI uri = buildUri(path, uriInfo.getQueryParameters());
        logger.info("Getting URI: '{}'", uri);

        final Request<?, com.atlassian.sal.api.net.Response> salRequest = buildSalRequest(uri, headers.getRequestHeaders());
        try
        {
            return salRequest.executeAndReturn(new JaxRsResponseHandler());
        }
        catch (ResponseException e)
        {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    private Request<?, com.atlassian.sal.api.net.Response> buildSalRequest(URI uri, MultivaluedMap<String, String> headers)
    {
        final Request<?, com.atlassian.sal.api.net.Response> salRequest = requestFactory.createRequest(Request.MethodType.GET, uri.toString());
        salRequest.setFollowRedirects(false);

        for (Map.Entry<String, List<String>> header : headers.entrySet())
        {
            for (String headerValue : header.getValue())
            {
                salRequest.addHeader(header.getKey(), headerValue);
            }
        }
        return salRequest;
    }

    private URI buildUri(String path, MultivaluedMap<String, String> queryParameters)
    {
        final UriBuilder uriBuilder = UriBuilder.fromUri(configurationManager.getApiBaseUrl()).path(version).path(path);
        for (Map.Entry<String, List<String>> queryParam : queryParameters.entrySet())
        {
            uriBuilder.queryParam(queryParam.getKey(), toArray(queryParam.getValue(), String.class));
        }
        if (!queryParameters.containsKey(AUTH_TOKEN_QUERY_PARAM))
        {
            uriBuilder.queryParam(AUTH_TOKEN_QUERY_PARAM, configurationManager.getApiToken().getOrElse(""));
        }
        return uriBuilder.build();
    }

    private static final class JaxRsResponseHandler implements ReturningResponseHandler<com.atlassian.sal.api.net.Response, Response>
    {
        private static final Predicate<Map.Entry<String, String>> BLACKLISTED_HEADERS = new Predicate<Map.Entry<String, String>>()
        {
            @Override
            public boolean apply(Map.Entry<String, String> input)
            {
                final String header = input.getKey().toLowerCase();
                //NB: we ommit some header from the response - it's causing https://jira.atlassian.com/browse/CONFDEV-28208
                return HttpHeaders.CONTENT_ENCODING.toLowerCase().equals(header) || "connection".equals(header) || "transfer-encoding".equals(header);
            }
        };

        @Override
        public Response handle(com.atlassian.sal.api.net.Response response) throws ResponseException
        {
            final Response.ResponseBuilder responseBuilder = Response.status(response.getStatusCode()).entity(response.getResponseBodyAsString());
            for (Map.Entry<String, String> h : filter(response.getHeaders().entrySet(), not(BLACKLISTED_HEADERS)))
            {
                logger.debug("adding header {}:{}", h.getKey(), h.getValue());
                responseBuilder.header(h.getKey(), h.getValue());
            }
            return responseBuilder.build();
        }
    }
}
