package com.atlassian.hipchat.plugins.core.rest.v1;

import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import com.atlassian.hipchat.plugins.core.rest.AbstractHipChatProxy;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;

import javax.ws.rs.Path;

@Path("{path: .*}")
@AnonymousAllowed
public final class HipChatProxy extends AbstractHipChatProxy
{
    public HipChatProxy(HipChatConfigurationManager configurationManager,
                        RequestFactory<Request<?, com.atlassian.sal.api.net.Response>> requestFactory)
    {
        super(configurationManager, requestFactory, "v1");
    }
}
