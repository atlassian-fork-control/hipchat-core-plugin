package com.atlassian.hipchat.plugins.core.client;

import com.atlassian.fugue.Either;
import com.atlassian.hipchat.plugins.api.client.ClientError;
import com.atlassian.httpclient.api.Response;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.transformValues;

public final class ClientUtils
{
    private static final Logger logger = LoggerFactory.getLogger(ClientUtils.class);

    static <T> Function<Response, Either<ClientError, T>> constantRight(T t)
    {
        return new ConstantRightFunction<T>(t);
    }

    static <T> Function<Response, Either<ClientError, T>> parseJson(String name, Class<T> type)
    {
        return new JsonFunction<T>(name, type);
    }

    static <T> Function<Response, Either<ClientError, T>> parseJson(String name, TypeReference<T> typeReference)
    {
        return new JsonFunction<T>(name, typeReference);
    }

    static <T> Function<Throwable, Either<ClientError, T>> throwable()
    {
        return new ThrowableFunction<T>();
    }

    static <T> Function<Response, Either<ClientError, T>> error()
    {
        return new ErrorFunction<T>();
    }

    static JsonParser getJsonParser(String json) throws IOException
    {
        return newObjectMapper().getJsonFactory().createJsonParser(json);
    }

    public static String urlEncode(Map<String, String> params)
    {
        return Joiner.on("&").withKeyValueSeparator("=").join(transformValues(params, new UrlEncodeFunction()));
    }

    private static ObjectMapper newObjectMapper()
    {
        return new ObjectMapper().configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private static final class ConstantRightFunction<T> implements Function<Response, Either<ClientError, T>>
    {
        private final T t;

        private ConstantRightFunction(T t)
        {
            this.t = t;
        }

        @Override
        public Either<ClientError, T> apply(Response r)
        {
            return Either.right(t);
        }
    }

    private static final class JsonFunction<T> implements Function<Response, Either<ClientError, T>>
    {
        private final String name;
        private final Class<T> type;
        private final TypeReference<T> typeReference;

        private JsonFunction(String name, Class<T> type)
        {
            this.name = checkNotNull(name);
            this.type = checkNotNull(type);
            this.typeReference = null;
        }

        private JsonFunction(String name, TypeReference<T> typeReference)
        {
            this.name = checkNotNull(name);
            this.type = null;
            this.typeReference = checkNotNull(typeReference);
        }

        @Override
        public Either<ClientError, T> apply(Response r)
        {
            final String json = r.getEntity();
            logger.debug("Parsing JSON: {}", json);
            try
            {
                final JsonParser jsonParser = getJsonParser(json);
                while (jsonParser.nextToken() != JsonToken.END_OBJECT)
                {
                    if (name.equals(jsonParser.getCurrentName()))
                    {
                        jsonParser.nextValue();
                        if (type != null)
                        {
                            return Either.right(jsonParser.readValueAs(type));
                        }
                        else
                        {
                            return Either.right(jsonParser.<T>readValueAs(typeReference));
                        }
                    }
                }
            }
            catch (IOException e)
            {
                return Either.left(new ClientError(e));
            }
            return Either.left(newJsonParsingClientError(json));
        }
    }

    private static ClientError newJsonParsingClientError(String json)
    {
        return new ClientError("Unable to parse Json:\n" + json, "-1", "Json parsing error"); // TODO use 'correct' code
    }

    private static final class ErrorFunction<T> implements Function<Response, Either<ClientError, T>>
    {
        @Override
        public Either<ClientError, T> apply(Response r)
        {
            final String json = r.getEntity();
            try
            {
                final JsonParser jsonParser = getJsonParser(json);
                while (jsonParser.nextToken() != JsonToken.END_OBJECT)
                {
                    if ("error".equals(jsonParser.getCurrentName()))
                    {
                        jsonParser.nextValue();
                        return Either.left(jsonParser.readValueAs(ClientError.class));
                    }
                }
            }
            catch (IOException e)
            {
                return Either.left(new ClientError(e));
            }
            return Either.left(newJsonParsingClientError(json));
        }
    }

    private static final class ThrowableFunction<T> implements Function<Throwable, Either<ClientError, T>>
    {
        @Override
        public Either<ClientError, T> apply(Throwable t)
        {
            return Either.left(new ClientError(t));
        }
    }

    private static final class UrlEncodeFunction implements Function<String, String>
    {
        @Override
        public String apply(String input)
        {
            try
            {
                return URLEncoder.encode(input, "UTF-8");
            }
            catch (UnsupportedEncodingException e)
            {
                throw new IllegalStateException(e);
            }
        }
    }
}
